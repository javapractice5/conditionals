import java.util.Scanner;

public class MenuRunner {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter number 1: ");
        int number1= scanner.nextInt();
       // System.out.println("Number1 is "+number1);
        System.out.print("Enter number 2: ");
        int number2= scanner.nextInt();

        System.out.println("Menu");
        System.out.println("1- Add");
        System.out.println("2- Subtract");
        System.out.println("3- Multiply");
        System.out.println("4- Divide");

        System.out.println("Enter your choice");
        int choice= scanner.nextInt();

        if(choice==1)
        {
            System.out.println("Result="+(number1+number2));
        } else if (choice==2) {
            System.out.println("Result="+(number1-number2));
        } else if (choice==3) {
            System.out.println("Result="+(number1*number2));
        } else if (choice==4) {
            System.out.println("Result="+(number1/number2));
        } else {
            System.out.println("Choice not available");
        }
    }
}
